package controllers;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class AdminWindController implements Initializable {


    @FXML
    Button exit;

    @FXML
    Button clientsInfo;

    @FXML
    Button clientOperations;


    @Override
    public void initialize(URL location, ResourceBundle resources) {

        exit.setOnAction(e-> {
            exit.getScene().getWindow().hide();
        });

        clientsInfo.setOnAction(e -> {
            Parent root = null;
            try {
                root = FXMLLoader.load(getClass().getResource("../FXML/clientsInfor.fxml"));
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            Stage stage = new Stage();
            stage.setScene(new Scene(root));
            stage.setTitle("Information about clients");
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.showAndWait();
        });

        clientOperations.setOnAction(e -> {
            Parent root = null;
            FXMLLoader loader = new FXMLLoader(getClass().getResource("../FXML/Transactions.fxml"));

            try {
                TransactionsController.setSolution("all");
                root = loader.load();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            Stage stage = new Stage();
            stage.setScene(new Scene(root));
            stage.setTitle("Information about transactions");
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.showAndWait();
        });


    }
}
