package controllers;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import main.Client;

import java.net.URL;
import java.util.ResourceBundle;

public class CheckBalanceController implements Initializable {

    private static CheckBalanceController instance;
    private static Stage wind;
    private static String mes;
    private static Client crtClient;

    @FXML
    private Button back;

    @FXML
    private Label rubls;

    @FXML
    private TextField textField;

    @FXML
    private Label infoLabel;

    public static CheckBalanceController getInstance() {
        if(instance == null) {
            instance = new CheckBalanceController();
        }
        return instance;
    }


    public  void createPage(String location, String str, Client crt) {
        mes = str;
        crtClient = crt;
        wind = new Stage();

        try {
            Parent root = FXMLLoader.load(getClass().getResource(location));
            Scene sc = new Scene(root);
            wind.setScene(sc);
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }

    public void showWindow() {
        if(wind != null) {
            wind.show();
        }
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        back.setOnAction(e ->
                back.getScene().getWindow().hide());
        infoLabel.setText(mes);
        if (mes.equals("Card number:")){
            textField.setText(Integer.toString(crtClient.getCardNumber()));
        } else {
            textField.setText(Integer.toString(crtClient.getBalance()) + " rubls");
        }
    }

}
