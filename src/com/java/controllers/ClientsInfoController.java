package controllers;

import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;
import main.Client;
import main.ClientReport;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.apache.log4j.Logger;

import java.io.*;
import java.net.URL;
import java.util.*;

public class ClientsInfoController implements Initializable {

    private static Logger log = Logger.getLogger(ClientsInfoController.class);

    @FXML
    private TableColumn<Client, String> surname;

    @FXML
    private TableColumn<Client, String> name;

    @FXML
    private TableColumn<Client, String> fathersName;

    @FXML
    private TableColumn<Client, String> phone;

    @FXML
    private TableColumn<Client, String> passport;

    @FXML
    private TableColumn<Client, Integer> cardNumb;

    @FXML
    private TableColumn<Client, Integer> age;

    @FXML
    private  TableColumn<Client, Integer> balance;

    @FXML
    private TableView<Client> table;

    @FXML
    private ObservableList<Client> data;

    @FXML
    Button addClient;

    @FXML
    Button reportToFile;

    @FXML
    Button safeToFile;

    @FXML
    Button deleteClient;

    @FXML
    Button editClient;

    @FXML
    Button transactions;

    private void getClientsFromFile(String filePath){
        try (Scanner in = new Scanner(new FileInputStream(filePath))){
            ObservableList<Client> crt = FXCollections.observableArrayList();

            while (in.hasNextLine()){
                String surname = in.next();
                String name = in.next();
                String fathersName = in.next();
                Integer age = in.nextInt();
                String phone = in.next();
                String passport = in.next();
                Integer cardNumb = in.nextInt();
                Integer balance = in.nextInt();
                String password = in.next();

                crt.add(new Client(surname, name, fathersName, age, phone, passport, cardNumb, balance, password));
            }
            data = crt;

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void writeToFile(String filePath)  {
        try( BufferedWriter out = new BufferedWriter( new FileWriter(new File (filePath)))){
            for (int i=0; i<data.size(); i++){
                out.write(data.get(i).toString());
                if (i != data.size() - 1)
                    out.write("\r\n");
            }
        }catch(IOException ex){
            ex.printStackTrace();
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        reportToFile.setVisible(false);
        deleteClient.setVisible(false);
        editClient.setVisible(false);
        transactions.setVisible(false);

        surname.setCellValueFactory((new PropertyValueFactory<Client, String>("surname")));
        name.setCellValueFactory((new PropertyValueFactory<Client, String>("name")));
        fathersName.setCellValueFactory((new PropertyValueFactory<Client, String>("fathersName")));
        age.setCellValueFactory((new PropertyValueFactory<Client, Integer>("age")));
        phone.setCellValueFactory((new PropertyValueFactory<Client, String>("phone")));
        passport.setCellValueFactory((new PropertyValueFactory<Client, String>("passport")));
        cardNumb.setCellValueFactory((new PropertyValueFactory<Client, Integer>("number")));
        balance.setCellValueFactory(new PropertyValueFactory<Client, Integer>("balance"));

        getClientsFromFile("D:\\JavaProjects\\oop\\src\\com\\java\\main\\clients.txt");
        log.info("Data was received");

        table.setItems(data);

        addClient.setOnAction(e -> {
            Parent root = null;
            try {
                root = FXMLLoader.load(getClass().getResource("../FXML/Registration.fxml"));
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            Stage primaryStage = new Stage();
            primaryStage.setScene(new Scene(root, 640, 400));
            primaryStage.initModality(Modality.APPLICATION_MODAL);
            primaryStage.showAndWait();

            Client newClient = RegistrationController.getResult();
            if (newClient != null)
                data.add(newClient);
        });

        reportToFile.setOnAction(e -> {
            TextInputDialog dialog = new TextInputDialog("D:\\JavaProjects\\oop\\src\\com\\java\\reports");
            dialog.setContentText("Please choose to file:");

            Optional<String> result = dialog.showAndWait();
            if (result.isPresent()) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                     //   createReport(result.get());
                        log.info("Report was created");
                    }
                }).start();
            }
        });

        safeToFile.setOnAction(e -> {
            TextInputDialog dialog = new TextInputDialog("D:\\JavaProjects\\oop\\src\\com\\java\\main\\clients.txt");
            dialog.setContentText("Please choose path to file:");

            Optional<String> result = dialog.showAndWait();
            if (result.isPresent()) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        writeToFile(result.get());
                        log.info("Writing to file");
                    }
                }).start();
            }
        });

        table.getSelectionModel().getSelectedItems().addListener(new ListChangeListener<Client>() {
            @Override
            public void onChanged(Change<? extends Client> c) {
                deleteClient.setVisible(true);
                editClient.setVisible(true);
                transactions.setVisible(true);
            }
        });

        deleteClient.setOnAction(e ->{
            data.remove(table.getSelectionModel().getSelectedItem());
        });

        editClient.setOnAction(e -> {
            Parent root = null;
            FXMLLoader loader = new FXMLLoader(getClass().getResource("../FXML/editClient.fxml"));

            try {
                 EditClientController.setParametrs(table.getSelectionModel().getSelectedItem());
                root = loader.load();
            } catch (IOException e1) {
                e1.printStackTrace();
            }

            Stage primaryStage = new Stage();
            primaryStage.setScene(new Scene(root));
            primaryStage.initModality(Modality.APPLICATION_MODAL);
            primaryStage.showAndWait();
            table.refresh();

        });

        transactions.setOnAction(e -> {
            Parent root = null;
            FXMLLoader loader = new FXMLLoader(getClass().getResource("../FXML/Transactions.fxml"));

            try {
                TransactionsController.setSolution(table.getSelectionModel().getSelectedItem().getPhone());
                root = loader.load();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            Stage stage = new Stage();
            stage.setScene(new Scene(root));
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.showAndWait();
        });

    }


//    private void createReport(String filePath){
//        ArrayList<ClientReport> crt = new ArrayList<ClientReport>();
//
//        for (Client client : data) {
//            crt.add(new ClientReport((client)));
//        }
//
//        try{
//            JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(crt);
//            JasperReport report  = JasperCompileManager.compileReport("D:\\JavaProjects\\oop\\src\\com\\java\\reports\\Report.jrxml");
//            JasperPrint print = net.sf.jasperreports.engine.JasperFillManager.fillReport(report, new HashMap<>(), dataSource);
//            JasperExportManager.exportReportToHtmlFile(print, filePath + "\\Report.html");
//            JasperExportManager.exportReportToPdfFile(print, filePath + "\\Report.pdf");
//        } catch (net.sf.jasperreports.engine.JRException e) {
//            e.printStackTrace();
//        }
//    }


}
