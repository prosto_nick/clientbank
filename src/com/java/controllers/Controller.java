package controllers;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import main.Client;
import main.Transaction;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.Scanner;

public class Controller  implements Initializable {

    private static Controller instance;
    ArrayList<Transaction> crtTransactions = new ArrayList<>();

    @FXML
    Button exit;

    @FXML
    Button checkBalance;

    @FXML
    Button addMoney;

    @FXML
    Button getMoney;

    @FXML
    Button knowCardNumb;

    @FXML
    Button transferMoney;

    private static String phone;
    private ArrayList<Client> data;
    private Client crtClient;

    @FXML
    void releaseExit() {
        exit.getScene().getWindow().hide();
    }

    public static void setPhone(String Phone) {
        phone = Phone;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        getClientsFromFile("D:\\JavaProjects\\oop\\src\\com\\java\\main\\clients.txt");
        getTransactions("D:\\JavaProjects\\oop\\src\\com\\java\\main\\transactions.txt");

        instance = this;

        exit.setOnAction(e -> {
            writeToFile("D:\\JavaProjects\\oop\\src\\com\\java\\main\\clients.txt");
            saveTransactions("D:\\JavaProjects\\oop\\src\\com\\java\\main\\transactions.txt");
            exit.getScene().getWindow().hide();
        });

        checkBalance.setOnAction(e -> {

            CheckBalanceController.getInstance().createPage("../FXML/checkBalanceWind.fxml", "Current balance:", crtClient);
            CheckBalanceController.getInstance().showWindow();
        });

        knowCardNumb.setOnAction(e -> {

            CheckBalanceController.getInstance().createPage("../FXML/checkBalanceWind.fxml", "Card number:", crtClient);
            CheckBalanceController.getInstance().showWindow();
        });

        addMoney.setOnAction(e ->{

            addOrGetMoneyController.getInstance().createPage("../FXML/getOrAddMoney.fxml", crtClient, "add", crtTransactions);
            addOrGetMoneyController.getInstance().showWindow();
        });

        getMoney.setOnAction(e ->{

            addOrGetMoneyController.getInstance().createPage("../FXML/getOrAddMoney.fxml", crtClient, "get", crtTransactions);
            addOrGetMoneyController.getInstance().showWindow();
        });

        transferMoney.setOnAction(e -> {
            TransferMoneyController.getInstance().createPage("../FXML/transferMoney.fxml", crtClient, data, crtTransactions);
            TransferMoneyController.getInstance().showWindow();
        });
    }

    private void getClientsFromFile(String filePath){
        try (Scanner in = new Scanner(new FileInputStream(filePath))){
            ArrayList<Client> crt = new ArrayList<>();


            while (in.hasNextLine()){
                String surname = in.next();
                String name = in.next();
                String fathersName = in.next();
                Integer age = in.nextInt();
                String number = in.next();
                String passport = in.next();
                Integer cardNumb = in.nextInt();
                Integer balance = in.nextInt();
                String password = in.next();

                Client client = new Client(surname, name, fathersName, age, number, passport, cardNumb, balance, password);
                crt.add(client);
                if(client.getPhone().equals(phone)) {
                    crtClient = client;
                }

            }
            data = crt;


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void getTransactions(String filePath){
        try (Scanner in = new Scanner(new FileInputStream(filePath))){
            ArrayList<Transaction> crt = new ArrayList<>();


            while (in.hasNextLine()){
                String owner = in.next();
                LocalDate date = new LocalDate(in.next());
                LocalTime time = new LocalTime(in.next());
                String operation = in.next();
                Integer amount = in.nextInt();
                if (operation.equals("transfer")) {
                    String receiver = in.next();
                    Transaction tr = new Transaction(owner, date, time, operation, amount, receiver);
                    crt.add(tr);
                }
                else {
                    Transaction tr = new Transaction(owner, date, time, operation, amount);
                    crt.add(tr);
                }

            }
            crtTransactions = crt;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void writeToFile(String filePath)  {
        try( BufferedWriter out = new BufferedWriter( new FileWriter(new File(filePath)))){
            for (int i=0; i<data.size(); i++){
                out.write(data.get(i).toString());
                if (i != data.size() - 1)
                    out.write("\r\n");
            }
        }catch(IOException ex){
            ex.printStackTrace();
        }
    }

    private void saveTransactions(String filePath) {
        try( BufferedWriter out = new BufferedWriter( new FileWriter(new File(filePath)))){
            for (int i=0; i<crtTransactions.size(); i++){
                out.write(crtTransactions.get(i).toString());
                if (i != crtTransactions.size() - 1)
                    out.write("\r\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}