package controllers;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import main.Client;

import java.net.URL;
import java.util.ResourceBundle;

public class EditClientController  implements Initializable {

    private static Client crtClient;

    @FXML
    TextField surnameTextField;

    @FXML
    TextField nameTextField;

    @FXML
    TextField fathersNameTextField;

    @FXML
    TextField ageTextField;


    @FXML
    TextField passportTextField;

    @FXML
    TextField amountTextField;

    @FXML
    Button okeyButton;

    @FXML
    Label infoLabel;

    public static  void setParametrs(Client crt){
        crtClient = crt;

    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {

        infoLabel.setVisible(false);

        surnameTextField.setText(crtClient.getSurname());
        nameTextField.setText(crtClient.getName());
        fathersNameTextField.setText(crtClient.getFathersName());
        ageTextField.setText(Integer.toString(crtClient.getAge()));
        passportTextField.setText(crtClient.getPassport());
        amountTextField.setText(Integer.toString(crtClient.getBalance()));


        okeyButton.setOnAction(e -> {
            infoLabel.setVisible(true);
            okeyButton.setVisible(false);
            crtClient.setSurname(surnameTextField.getText());
            crtClient.setName(nameTextField.getText());
            crtClient.setFathersName(fathersNameTextField.getText());
            crtClient.setAge(Integer.parseInt(ageTextField.getText()));
            crtClient.setPassport(passportTextField.getText());
            crtClient.setBalance(Integer.parseInt(amountTextField.getText()));

            infoLabel.setVisible(true);
            okeyButton.setVisible(false);
        });


    }
}
