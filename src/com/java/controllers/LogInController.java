package controllers;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Modality;
import javafx.stage.Stage;
import main.Client;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.ResourceBundle;
import java.util.Scanner;

public class LogInController implements Initializable {


    @FXML
    Button exit;

    @FXML
    Button admin;

    @FXML
    Button registration;

    @FXML
    TextField logTextField;

    @FXML
    PasswordField passTextField;

    @FXML
    Button inButton;

    private HashMap<String, String> data = new HashMap<>();

    @FXML
    void releaseExit() {
        exit.getScene().getWindow().hide();
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {

        getNumbsAndPasswords("D:\\JavaProjects\\oop\\src\\com\\java\\main\\clients.txt");


        inButton.setOnAction(e -> {
            if (isValidData()) {
                Parent root = null;
                FXMLLoader loader = new FXMLLoader(getClass().getResource("../FXML/sample.fxml"));

                try {
                    Controller.setPhone(logTextField.getText());
                    root = loader.load();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }


                Stage primaryStage = new Stage();
                primaryStage.setScene(new Scene(root));
                primaryStage.initModality(Modality.APPLICATION_MODAL);
                primaryStage.showAndWait();
            }else{
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setHeaderText("Error!");
                alert.setContentText("Wrong login or password!");
                alert.showAndWait();
            }
        });


        admin.setOnAction(e -> {
            Parent root = null;
            try {
                root = FXMLLoader.load(getClass().getResource("../FXML/adminKey.fxml"));
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            Stage stage = new Stage();
            stage.setScene(new Scene(root));
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.showAndWait();

        });

        registration.setOnAction(e -> {
            Parent root = null;
            FXMLLoader loader = new FXMLLoader(getClass().getResource("../FXML/Registration.fxml"));

            try {
                RegistrationController.setData(data);
                root = loader.load();
            } catch (IOException e1) {
                e1.printStackTrace();
            }

            Stage stage = new Stage();
            stage.setScene(new Scene(root));
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.showAndWait();
        });
    }


    private void getNumbsAndPasswords(String filePath) {
        try (Scanner in = new Scanner(new FileInputStream(filePath))) {

            while (in.hasNextLine()) {
                String surname = in.next();
                String name = in.next();
                String fathersName = in.next();
                Integer age = in.nextInt();
                String phone = in.next();
                String passport = in.next();
                Integer cardNumb = in.nextInt();
                Integer balance = in.nextInt();
                String password = in.next();

                data.put(phone, password);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }


    public boolean isValidData() {
        if (!data.containsKey(logTextField.getText())) {
            return false;
        } else {
            return data.get(logTextField.getText()).equals(passTextField.getText());
        }
    }

}