package controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Modality;
import javafx.stage.Stage;
import main.Client;
import main.Main;
import org.apache.log4j.Logger;

import java.io.*;
import java.net.URL;
import java.util.*;

public class RegistrationController implements Initializable {

    private static Logger log = Logger.getLogger(RegistrationController.class);
    @FXML
    Button createCard;
    @FXML
    TextField surnameTextField;
    @FXML
    TextField fathersNameTextField;
    @FXML
    TextField nameTextField;
    @FXML
    TextField ageTextField;
    @FXML
    TextField phoneTextField;
    @FXML
    TextField passportTextField;

    @FXML
    PasswordField passwordTextField;

    @FXML
    Label label;
    @FXML
    Label incorrectName;
    @FXML
    Label incorrectSurname;
    @FXML
    Label incorrectFathsName;
    @FXML
    Label incorrectAge;
    @FXML
    Label incorrectPhone;
    @FXML
    Label incorrectPassport;

    private ArrayList<Client> data;

    private static Client result;
    private  static HashMap<String, String> numbsAndPasswords;

    private String fioPattern = "[A-Z]{1}[a-z]{2,20}";
    private String agePattern = "([1-9]{1}[8-9]{1})|([2-9]{1}[0-9]{1})";
    private String phonePattern = "[0-9]{6,11}";
    private String passportPattern = "[0-9]{10}";
    private String passwordPattern = "[\\w]{3,20}";

    public RegistrationController() {
    }

    public static void setData(HashMap<String, String> data){
        numbsAndPasswords = data;
    }


    public void initialize(URL location, ResourceBundle resources) {
        getClientsFromFile("D:\\JavaProjects\\oop\\src\\com\\java\\main\\clients.txt");
        this.incorrectSurname.setVisible(false);
        this.incorrectName.setVisible(false);
        this.incorrectFathsName.setVisible(false);
        this.incorrectAge.setVisible(false);
        this.incorrectPhone.setVisible(false);
        this.incorrectPassport.setVisible(false);

        this.surnameTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (this.surnameTextField.getCharacters().toString().matches(this.fioPattern)) {
                this.surnameTextField.setStyle("-fx-text-box-border: green ; -fx-focus-color: green");
                this.incorrectSurname.setVisible(false);

            } else {
                this.surnameTextField.setStyle("-fx-text-box-border: red ; -fx-focus-color: red");
                this.incorrectSurname.setVisible(true);
            }

        });

        this.nameTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (this.nameTextField.getCharacters().toString().matches(this.fioPattern)) {
                this.nameTextField.setStyle("-fx-text-box-border: green ; -fx-focus-color: green");
                this.incorrectName.setVisible(false);

            } else {
                this.nameTextField.setStyle("-fx-text-box-border: red ; -fx-focus-color: red");
                this.incorrectName.setVisible(true);

            }

        });

        this.fathersNameTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (this.fathersNameTextField.getCharacters().toString().matches(this.fioPattern)) {
                this.fathersNameTextField.setStyle("-fx-text-box-border: green ; -fx-focus-color: green");
                this.incorrectFathsName.setVisible(false);

            } else {
                this.fathersNameTextField.setStyle("-fx-text-box-border: red ; -fx-focus-color: red");
                this.incorrectFathsName.setVisible(true);

            }
        });

        this.ageTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (this.ageTextField.getCharacters().toString().matches(this.agePattern)) {
                this.ageTextField.setStyle("-fx-text-box-border: green ; -fx-focus-color: green");
                this.incorrectAge.setVisible(false);

            } else {
                this.ageTextField.setStyle("-fx-text-box-border: red ; -fx-focus-color: red");
                this.incorrectAge.setVisible(true);

            }
        });

        this.phoneTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (this.phoneTextField.getCharacters().toString().matches(this.phonePattern)) {
                if (isUnicNumber()){
                    this.phoneTextField.setStyle("-fx-text-box-border: green ; -fx-focus-color: green");
                    this.incorrectPhone.setVisible(false);

                } else{

                    this.incorrectPhone.setText("This number already exists in base!");
                    this.incorrectPhone.setVisible(true);
                }

            } else {
                this.phoneTextField.setStyle("-fx-text-box-border: red ; -fx-focus-color: red");
                this.incorrectPhone.setVisible(true);
                this.incorrectPhone.setText("Try again!");
            }
        });

        this.passportTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (this.passportTextField.getCharacters().toString().matches(this.passportPattern)) {
                this.passportTextField.setStyle("-fx-text-box-border: green ; -fx-focus-color: green");
                this.incorrectPassport.setVisible(false);

            } else {
                this.passportTextField.setStyle("-fx-text-box-border: red ; -fx-focus-color: red");
                this.incorrectPassport.setVisible(true);
            }
        });

        this.passwordTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (this.passwordTextField.getCharacters().toString().matches(this.passwordPattern)) {
                this.passwordTextField.setStyle("-fx-text-box-border: green ; -fx-focus-color: green");
            } else{
                this.passwordTextField.setStyle("-fx-text-box-border: red ; -fx-focus-color: red");
            }
        });

        result = null;
        createCard.setOnAction(e -> {
            try {
                checkFields();
                result = new Client(
                        this.surnameTextField.getText(),
                        this.nameTextField.getText(),
                        this.fathersNameTextField.getText(),
                        Integer.parseInt(this.ageTextField.getText()),
                        this.phoneTextField.getText(),
                        this.passportTextField.getText(),
                        this.passwordTextField.getText());

                        data.add(result);
                        numbsAndPasswords.put(phoneTextField.getText(), this.passwordTextField.getText());
                        writeToFile("D:\\JavaProjects\\oop\\src\\com\\java\\main\\clients.txt");
                        this.createCard.getScene().getWindow().hide();

            } catch (Exception ex) {
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setHeaderText("Error!");
                log.warn(ex.toString());
                alert.setContentText(ex.getMessage());
                alert.showAndWait();
            }
        });
    }

    public static Client getResult() {
        return result;
    }


    private void checkFields() throws InvalidStringValueException, InvalidIntegerValueException {
        if(!nameTextField.getCharacters().toString().matches(fioPattern)
                || !surnameTextField.getCharacters().toString().matches(fioPattern)
                || !fathersNameTextField.getCharacters().toString().matches(fioPattern)){
             throw new InvalidStringValueException();
        }

        if(!ageTextField.getCharacters().toString().matches(agePattern)
            || !phoneTextField.getCharacters().toString().matches(phonePattern)
            || !passportTextField.getCharacters().toString().matches(passportPattern)){
            throw new InvalidIntegerValueException();
        }
    }

    private class InvalidStringValueException extends Exception{
        public InvalidStringValueException(){
            super("One of text field is wrong!");
        }
    }

    private class InvalidIntegerValueException extends Exception{
        public InvalidIntegerValueException(){
            super("One of digital field is wrong!");
        }
    }

    private void getClientsFromFile(String filePath){
        try (Scanner in = new Scanner(new FileInputStream(filePath))){
            ArrayList<Client> crt = new ArrayList<>();


            while (in.hasNextLine()){
                String surname = in.next();
                String name = in.next();
                String fathersName = in.next();
                Integer age = in.nextInt();
                String phone = in.next();
                String passport = in.next();
                Integer cardNumb = in.nextInt();
                Integer balance = in.nextInt();
                String password = in.next();

                crt.add(new Client(surname, name, fathersName, age, phone, passport, cardNumb, balance, password));
            }
         data = crt;


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public boolean isUnicNumber(){
        for (int i=0; i<data.size(); i++){
            if (phoneTextField.getText().equals( data.get(i).getPhone())){
                return false;
            }
        }
        return true;

    }

    private void writeToFile(String filePath)  {
        try( BufferedWriter out = new BufferedWriter( new FileWriter(new File(filePath)))){
            for (int i=0; i<data.size(); i++){
                out.write(data.get(i).toString());
                if (i != data.size() - 1)
                    out.write("\r\n");
            }
        }catch(IOException ex){
            ex.printStackTrace();
        }
    }

}
