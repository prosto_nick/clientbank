package controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import main.Client;
import main.Transaction;

import javax.swing.text.TabableView;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.net.URL;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.Scanner;

public class TransactionsController implements Initializable {

    @FXML
    Button searchByAmount;

    @FXML
    Button searchByOperation;

    @FXML
    Label from;

    @FXML
    Label to;

    @FXML
    Label operationLabel;

    @FXML
    Label moneyLabel;

    @FXML
    Button okeyButton;

    @FXML
    TextField textField;

    @FXML
    TextField textField2;

    @FXML
    Button okButton;

    private static String solution;

    @FXML
    private TableView<Transaction>  table;

    @FXML
    private TableColumn<Transaction, String> owner;

    @FXML
    private TableColumn<Transaction, LocalDate> date;

    @FXML
    private  TableColumn<Transaction, LocalTime> time;

    @FXML
    private TableColumn<Transaction, String> operation;

    @FXML
    private TableColumn<Transaction, Integer> amount;

    @FXML
    private TableColumn<Transaction, String> receiver;

    @FXML
    private ObservableList<Transaction> data;

    @FXML
    private  ObservableList<Transaction> operations;


    public static void setSolution(String str){
        solution = str;
    }

    private void getTransactions(String filePath){
        try (Scanner in = new Scanner(new FileInputStream(filePath))){
            ObservableList<Transaction> crt = FXCollections.observableArrayList();;

            while (in.hasNextLine()){
                String owner = in.next();
                org.joda.time.LocalDate date = new org.joda.time.LocalDate(in.next());
                org.joda.time.LocalTime time = new org.joda.time.LocalTime(in.next());
                String operation = in.next();
                Integer amount = in.nextInt();
                if (operation.equals("transfer")) {
                    String receiver = in.next();
                    Transaction tr = new Transaction(owner, date, time, operation, amount, receiver);
                    if (solution.equals("all")) {
                        crt.add(tr);
                    }else{
                        if (owner.equals(solution))
                            crt.add(tr);
                    }
                }
                else {
                    Transaction tr = new Transaction(owner, date, time, operation, amount);
                    if (solution.equals("all")) {
                        crt.add(tr);
                    }else{
                        if (owner.equals(solution))
                            crt.add(tr);
                    }
                }

            }
            data = crt;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void getTransactionsByType(String str){
        operations = FXCollections.observableArrayList();
        for (int i=0; i<data.size(); i++){
            if (data.get(i).getOperation().equals(str)){
                operations.add(data.get(i));
            }
        }
    }

    private  void getTransactionsByMoney(int min, int max){
        operations = FXCollections.observableArrayList();
        for (int i=0; i<data.size(); i++) {
            if(data.get(i).getAmount() >= min &&
                    data.get(i).getAmount() <= max){
                operations.add(data.get(i));
            }
        }
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {

        from.setVisible(false);
        to.setVisible(false);
        textField.setVisible(false);
        textField2.setVisible(false);
        moneyLabel.setVisible(false);
        operationLabel.setVisible(false);
        okeyButton.setVisible(false);
        okButton.setVisible(false);

        owner.setCellValueFactory(new PropertyValueFactory<Transaction, String>("owner"));
        date.setCellValueFactory(new PropertyValueFactory<Transaction, LocalDate>("date"));
        time.setCellValueFactory(new PropertyValueFactory<Transaction, LocalTime>("time"));
        operation.setCellValueFactory(new PropertyValueFactory<Transaction, String>("operation"));
        amount.setCellValueFactory(new PropertyValueFactory<Transaction, Integer>("amount"));
        receiver.setCellValueFactory(new PropertyValueFactory<Transaction, String>("receiver"));

        getTransactions("D:\\JavaProjects\\oop\\src\\com\\java\\main\\Transactions.txt");
        table.setItems(data);

        searchByOperation.setOnAction(e -> {
            from.setVisible(false);
            to.setVisible(false);
            textField2.setVisible(false);
            moneyLabel.setVisible(false);
            okeyButton.setVisible(false);
            operationLabel.setVisible(true);
            textField.setVisible(true);
            okButton.setVisible(true);
            textField.setText("add");
        });

        searchByAmount.setOnAction(e -> {
            operationLabel.setVisible(false);
            okButton.setVisible(false);
            from.setVisible(true);
            to.setVisible(true);
            textField.setVisible(true);
            textField2.setVisible(true);
            moneyLabel.setVisible(true);
            okeyButton.setVisible(true);
            textField.setText("0");
            textField2.setText("100000");
        });

        okButton.setOnAction(e -> {
           if (textField.getText().equals("add") ||
                   textField.getText().equals("get" )||
                   textField.getText().equals("transfer")) {
                getTransactionsByType(textField.getText());
                table.setItems(operations);
           }
           else{
               Alert alert = new Alert(Alert.AlertType.INFORMATION);
               alert.setHeaderText("Error!");
               alert.setContentText("Wrong type of operation!");
               alert.showAndWait();
               textField.setText("");
           }
        });

        okeyButton.setOnAction(e ->{
            if(Integer.parseInt(textField.getText()) >= 0 &&
                    Integer.parseInt(textField2.getText()) >= 0){
                getTransactionsByMoney(Integer.parseInt(textField.getText()),Integer.parseInt(textField2.getText()));
                table.setItems(operations);
            }else {
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setHeaderText("Error!");
                alert.setContentText("Invalid range!");
                alert.showAndWait();

            }
        });



    }
}
