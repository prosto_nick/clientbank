package controllers;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import main.Client;
import main.Transaction;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class TransferMoneyController implements Initializable {

    private String pattern = "[1-9]{1}[0-9]{2,7}";

    @FXML
    Button back;

    @FXML
    Button okPhoneButton;

    @FXML
    Button okMoneyButton;

    @FXML
    TextField phoneTextField;

    @FXML
    TextField moneyTextField;

    @FXML
    Label infoLabel;

    @FXML
    Label moneyLabel;

    private static TransferMoneyController instance;
    private static Stage wind;
    private static Client crtClient;
    private Client receiver;
    private static ArrayList<Transaction> crt;

    boolean isValid = false;
    boolean isGood = false;

   private static ArrayList<Client> clients;

    public static TransferMoneyController getInstance() {
        if(instance == null) {
            instance = new TransferMoneyController();
        }
        return instance;
    }



    public  void createPage(String location, Client crt, ArrayList<Client> data, ArrayList<Transaction> tr) {
        crtClient = crt;
        this.crt = tr;
        clients = data;
        wind = new Stage();

        try {
            Parent root = FXMLLoader.load(getClass().getResource(location));
            Scene sc = new Scene(root);
            wind.setScene(sc);
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }


    public void showWindow() {
        if(wind != null) {
            wind.show();
        }
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {

        infoLabel.setVisible(false);
        moneyLabel.setVisible(false);
        okMoneyButton.setVisible(false);
        moneyTextField.setVisible(false);
        back.setOnAction(e ->
                back.getScene().getWindow().hide());

        this.moneyTextField.textProperty().addListener((observable1, oldValue1, newValue1) -> {
            if (this.moneyTextField.getCharacters().toString().matches(this.pattern))
                isValid = true;
        });

        okPhoneButton.setOnAction(e -> {
            for (int i=0; i<clients.size(); i++){
                if (phoneTextField.getText().equals(clients.get(i).getPhone())) {
                    isGood = true;
                    receiver = clients.get(i);
                    moneyLabel.setVisible(true);
                    moneyTextField.setVisible(true);
                    okMoneyButton.setVisible(true);
                    okPhoneButton.setVisible(false);
                }
            }
            if (isGood)
                this.phoneTextField.setStyle("-fx-text-box-border: green ; -fx-focus-color: green");
            else{
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setHeaderText("Error!");
                alert.setContentText("Client with id " + phoneTextField.getText() + " isn't in our base!");
                alert.showAndWait();
            }

        });


        okMoneyButton.setOnAction(e -> {
            if (Integer.parseInt(moneyTextField.getText()) < crtClient.getBalance() && isValid){
                this.moneyTextField.setStyle("-fx-text-box-border: green ; -fx-focus-color: green");
                infoLabel.setVisible(true);
                crtClient.getMoney(Integer.parseInt(moneyTextField.getText()));
                receiver.setMoney(Integer.parseInt(moneyTextField.getText()));
                okMoneyButton.setVisible(false);
                Transaction tr = new Transaction(crtClient.getPhone(), "transfer",
                        Integer.parseInt(moneyTextField.getText()), phoneTextField.getText());
                crt.add(tr);
            }
            else {
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setHeaderText("Error!");
                alert.setContentText("Not enough money in your card!");
                alert.showAndWait();
            }
        });

    }
}
