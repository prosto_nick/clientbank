package controllers;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

public class UniversalController implements Initializable {

    private static UniversalController instance;
    private static Stage wind;
    private static String mes;

    @FXML
    Button back;

    @FXML
    Label infoLabel;

    public static UniversalController getInstance() {
        if (instance == null) {
            instance = new UniversalController();
        }
        return instance;
    }

    public  void createPage(String location, String str) {
        mes = str;
        wind = new Stage();

        try {
            Parent root = FXMLLoader.load(getClass().getResource(location));
            Scene sc = new Scene(root);
            wind.setScene(sc);
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }

    public void showWindow() {
        if(wind != null) {
            wind.show();
        }
    }



    @Override
    public void initialize(URL location, ResourceBundle resources) {
        back.setOnAction(e ->
                back.getScene().getWindow().hide());
        infoLabel.setText(mes);
    }
}
