package controllers;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import main.Client;
import main.Transaction;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class addOrGetMoneyController implements Initializable {

    private static addOrGetMoneyController instance;
    private static Stage wind;
    private static Client crtClient;
    private boolean isValid = false;
    private static String str;
    private  static   ArrayList<Transaction> crt;

    private String pattern = "[1-9]{1}[0-9]{2,7}";

    @FXML
    Button back;

    @FXML
    Button OK;

    @FXML
    Label infoLabel;

    @FXML
    TextField textField;

    public static addOrGetMoneyController getInstance() {
        if(instance == null) {
            instance = new addOrGetMoneyController();
        }
        return instance;
    }

    public  void createPage(String location, Client crt, String string, ArrayList<Transaction> tr) {
        str = string;
        crtClient = crt;
        this.crt = tr;
        wind = new Stage();

        try {
            Parent root = FXMLLoader.load(getClass().getResource(location));
            Scene sc = new Scene(root);
            wind.setScene(sc);
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }

    public void showWindow() {
        if(wind != null) {
            wind.show();
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.infoLabel.setVisible(false);


        this.textField.textProperty().addListener((observable1, oldValue1, newValue1) -> {
            if (this.textField.getCharacters().toString().matches(this.pattern)) {
                this.textField.setStyle("-fx-text-box-border: green ; -fx-focus-color: green");
                isValid = true;
            }
            else
                this.textField.setStyle("-fx-text-box-border: red ; -fx-focus-color: red");
        });

        OK.setOnAction(e -> {
            if (isValid) {

                if (str.equals("add")){
                    crtClient.setMoney(Integer.parseInt(textField.getText()));
                    infoLabel.setVisible(true);
                    OK.setVisible(false);
                    Transaction tr = new Transaction(crtClient.getPhone(), "add", Integer.parseInt(textField.getText()));
                    crt.add(tr);

                }
                else {
                    if (Integer.parseInt(textField.getText()) < crtClient.getBalance()) {
                        crtClient.getMoney(Integer.parseInt(textField.getText()));
                        infoLabel.setVisible(true);
                        OK.setVisible(false);
                        Transaction tr = new Transaction(crtClient.getPhone(), "get", Integer.parseInt(textField.getText()));

                        crt.add(tr);
                    }
                    else{
                        Alert alert = new Alert(Alert.AlertType.INFORMATION);
                        alert.setHeaderText("Error!");
                        alert.setContentText("Not enough money");
                        alert.showAndWait();
                    }
                }
            }
            else
                this.textField.setStyle("-fx-text-box-border: red ; -fx-focus-color: red");
        });

         back.setOnAction(e ->
                back.getScene().getWindow().hide());
    }
}
