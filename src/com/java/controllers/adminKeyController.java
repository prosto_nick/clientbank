package controllers;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class adminKeyController implements Initializable {

    private String key = "1111";

    @FXML
    PasswordField keyTextField;

    @FXML
    Button okButton;

    @FXML
    Label incorrectKey;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        incorrectKey.setVisible(false);

        okButton.setOnAction(e -> {
            if (isValidKey()){
                this.keyTextField.setStyle("-fx-text-box-border: green ; -fx-focus-color: green");
                incorrectKey.setVisible(false);
                Parent root = null;
                try {
                    root = FXMLLoader.load(getClass().getResource("../FXML/adminWind.fxml"));
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
                Stage stage = new Stage();
                stage.setScene(new Scene(root));
                stage.initModality(Modality.APPLICATION_MODAL);
                stage.showAndWait();
            }else{
               this.keyTextField.setStyle("-fx-text-box-border: red ; -fx-focus-color: red");
               incorrectKey.setVisible(true);
            }
        });


    }

    public boolean isValidKey(){
        return keyTextField.getText().equals(key);
    }
}
