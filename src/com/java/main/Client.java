package main;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.util.Random;

public class Client {
    private StringProperty name;
    private StringProperty surname;
    private StringProperty fathersName;
    private StringProperty phone;
    private StringProperty passport;
 //   private IntegerProperty cardNumb;
    private IntegerProperty age;
    private String password;

    Card card;

    public Client() {
    }

    public Client(String Surname, String Name, String FathersName,
                  Integer Age, String Phone, String Passport, String Password) {

        this.surname = new SimpleStringProperty(Surname);
        this.name = new SimpleStringProperty(Name);
        this.fathersName = new SimpleStringProperty(FathersName);
        this.phone = new SimpleStringProperty(Phone);
        this.passport = new SimpleStringProperty(Passport);
        this.age = new SimpleIntegerProperty(Age);
        //TODO CARDNumb
      //  this.cardNumb = new SimpleIntegerProperty(MAX_CARD_NUMB);
        this.password = Password;
        this.card = new Card();

    }

    public Client(String Surname, String Name, String FathersName,
                  Integer Age, String Phone, String Passport, Integer CardNumb, Integer Balance, String Password) {

        this.surname = new SimpleStringProperty(Surname);
        this.name = new SimpleStringProperty(Name);
        this.fathersName = new SimpleStringProperty(FathersName);
        this.phone = new SimpleStringProperty(Phone);
        this.passport = new SimpleStringProperty(Passport);
        this.age = new SimpleIntegerProperty(Age);
     //   this.cardNumb = new SimpleIntegerProperty(CardNumb);
        this.password = Password;
        this.card = new Card(CardNumb, Balance);
    }

    @Override
    public String toString(){
        return surname.get() + " " + name.get() + " " + fathersName.get() + " "
                + age.get() + " " + phone.get() + " " + passport.get() + " " +
                card.getNumb() + " " + card.getBalance() + " " + password;
    }

    public String getName() {
        return name.get();
    }

    public StringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public String getSurname() {
        return surname.get();
    }

    public void setSurname(String surname) {this.surname.set(surname);}

    public String getPassword() {return password;}

    public String getFathersName() {
        return fathersName.get();
    }

    public void setFathersName(String fathersName) {this.fathersName.set(fathersName);}

    public String getPhone() {
        return phone.get();
    }


    public void setPhone(String phone) {
        this.phone.set(phone);
    }

    public String getPassport() {
        return passport.get();
    }

    public void setPassport(String passport) {
        this.passport.set(passport);
    }

    public int getAge() {
        return age.get();
    }

    public void getMoney(int amount) {
        if (card.getBalance() > amount){
            card.setBalance(getBalance() - amount);
        }
    }

    public void setBalance(Integer balance){card.setBalance(balance);}

   public void setMoney(int amount) {card.setBalance(getBalance() + amount);}

   public int getCardNumber(){return card.getNumb();}

   public int getBalance() {return card.getBalance();}



    public IntegerProperty numberProperty() {
        return card.numberProperty();
    }

    public IntegerProperty balanceProperty() {
        return card.balanceProperty();
    }

    public void setAge(int age) {
        this.age.set(age);
    }

    public class Card{
        private IntegerProperty number;
        private IntegerProperty balance;
        int max = 99999999;
        int min = 10000000;



        Card(Integer Numb, Integer Balance){
            this.number = new SimpleIntegerProperty(Numb);
            this.balance = new SimpleIntegerProperty(Balance);
        }

        Card(){
            max -= min;
           this.number = new SimpleIntegerProperty( (int)(Math.random() * ++max) + min);
           this.balance = new SimpleIntegerProperty(0);

        }

        IntegerProperty numberProperty() {
            return number;
        }

        IntegerProperty balanceProperty() {
            return balance;
        }

        int getBalance() {return balance.get(); }

        void setBalance(int money) {this.balance = new SimpleIntegerProperty(money); }

        int getNumb() {return number.get();}
    }
}

