package main;

import java.util.Random;



public class ClientReport {
    private String name;
    private String surname;
    private String fathersName;
    private String phone;
    private String passport;
    private Integer cardNumb;
    private Integer age;
    private static int MAX_CARD_NUMB = 99999999;
    private String password;


    public ClientReport() {
    }

    public ClientReport(Client toCopy){
        this.surname = toCopy.getSurname();
        this.name = toCopy.getName();
        this.fathersName = toCopy.getFathersName();
        this.age = toCopy.getAge();
        this.phone = toCopy.getPhone();
        this.passport = toCopy.getPassport();
      //  this.cardNumb = toCopy.getCardNumb();
        this.password = toCopy.getPassword();

    }

    public ClientReport(String Surname, String Name, String FathersName,
                        Integer Age, String Phone, String Passport, Integer CardNumb, String Password) {

        this.surname = Surname;
        this.name = Name;
        this.fathersName = FathersName;
        this.phone = Phone;
        this.passport = Passport;
        this.age = Age;
        this.cardNumb = CardNumb;
        this.password = Password;
    }


    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getFathersName() {
        return fathersName;
    }

    public String getPhone() {
        return phone;
    }

    public String getPassport() {
        return passport;
    }

    public Integer getCardNumb() {
        return cardNumb;
    }

    public Integer getAge() {
        return age;
    }

    public  String getPassword() {return password;}
}
