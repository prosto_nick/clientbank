package main;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.apache.log4j.Logger;


public class Main extends Application {

    private static Logger log = Logger.getLogger(Main.class);

    @Override
    public void start(Stage primaryStage) throws Exception{

        log.info("application start");
        //TODO Russian symbols
     //   Parent root = FXMLLoader.load(getClass().getResource("D:\\JavaProjects\\oop\\src\\com\\java\\FXML\\clientsInfor.fxml"));
        Parent root = FXMLLoader.load(getClass().getResource("../FXML/LogIn.fxml"));
        primaryStage.setScene(new Scene(root, 450, 310));
        primaryStage.show();


    }


    public static void main(String[] args) {
        launch(args);
    }
}

