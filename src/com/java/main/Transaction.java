package main;


import jdk.nashorn.internal.objects.annotations.Property;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;

public class Transaction {

    private String owner;
    private String operation;
    private String receiver="";
    private  int amount;
    LocalDate date;
    LocalTime time;


    @Override
    public  String toString(){
        if (receiver.equals(""))
            return owner + " " + date + " " + time + " " + operation
                + " " + amount;
        else
            return owner + " " + date + " " + time + " " + operation
                    + " " + amount + " " + receiver;
    }

   public  Transaction(String from, String operationType, int amount){
        LocalDate  localDate = new LocalDate();
        date = localDate;
        LocalTime localTime = new LocalTime();
        time = localTime;
        this.owner = from;
        this.operation = operationType;
        this.amount = amount;
    }

    public  Transaction(String from, LocalDate date, LocalTime time, String operationType, int amount){
        this.date = date;
        this.time = time;
        this.owner = from;
        this.operation = operationType;
        this.amount = amount;

    }

    public Transaction(String from, String operationType, int amount, String receiver){
        LocalDate  localDate = new LocalDate();
        date = localDate;
        LocalTime localTime = new LocalTime();
        time = localTime;
        this.owner = from;
        this.operation = operationType;
        this.receiver = receiver;
        this.amount = amount;
    }

    public  Transaction(String from, LocalDate date, LocalTime time, String operationType, int amount, String receiver){
        this.date = date;
        this.time = time;
        this.owner = from;
        this.operation = operationType;
        this.amount = amount;
        this.receiver = receiver;

    }

    public String getOwner() {
        return owner;
    }

    public String getOperation() {
        return operation;
    }

    public String getReceiver() {
        return receiver;
    }

    public int getAmount() {
        return amount;
    }

    public LocalDate getDate() {
        return date;
    }

    public LocalTime getTime() {
        return time;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public void setOperation(String operationType) {
        this.operation = operationType;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public void setTime(LocalTime time) {
        this.time = time;
    }
}
