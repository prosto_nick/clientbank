package main;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ClientTest {

        @Test
        void constructorTest(){
            Client client = new Client("Ivanov", "Petr", "Andreevich",
                    32, "89097456897", "1611735476", "opu76g");

            assertEquals("Petr", client.getName());
            assertEquals(32, client.getAge());
            assertEquals("1611735476", client.getPassport());
        }

        @Test
        void checkNPEClient(){
            Client client = null;
            assertThrows(NullPointerException.class, () -> client.toString());
        }
}